#!/usr/bin/env python3

import argparse
import math
import os
import random
import string

# https://docs.python.org/3.8/library/subprocess.html
import subprocess
import sys


def main(argv=None):
    if argv is None:
        argv = sys.argv

    parser = argparse.ArgumentParser(description="Produce a plaque of a font-file")
    parser.add_argument(
        "--lower",
        action="store_true",
        default=False,
        help="Restrict to lower case latin",
    )
    parser.add_argument(
        "--upper",
        action="store_true",
        default=False,
        help="Restrict to upper case latin",
    )
    parser.add_argument(
        "--order", default="shuffle", help="How to order the glyphs: code/shuffle/uca"
    )
    parser.add_argument(
        "--unicodes",
        action="store_true",
        default=False,
        help="Output unicodes on stdout, instead of running hb-view",
    )
    parser.add_argument(
        "--chars",
        action="store_true",
        default=False,
        help="Output characters on stdout, instead of running hb-view",
    )
    parser.add_argument("fontfile", help="font file (TTF or OTF) to examine")

    args, rest = parser.parse_known_args(argv[1:])

    unicodes = list_unicodes(args.fontfile)

    # Empty set implies no restrictions;
    # non-empty set means restricted to contents of set (plus U+000A)
    restrict = set()

    if args.lower:
        # Restrict to lowercase;
        lc = (
            # simple
            "aæbcdðeəʒfghijkĸlmnɲŋoœpþqrsꞌßſtuvwxyz"
            +
            # composite
            "ø"
        )
        lower_unicodes = set(ord(x) for x in lc)
        restrict |= lower_unicodes

    if args.upper:
        # Restrict to uppercase;
        uc = (
            # simple
            "AÆBCDEƏƷFGHIJKLMNƝŊOŒPÞQRSꞋẞTUVWXYZ"
            +
            # composite
            "ÐØ"
        )

        upper_unicodes = set(ord(x) for x in uc)
        restrict |= upper_unicodes

    if restrict:
        # some care is required to compare codepoints numerically.
        restrict |= set([10])
        unicodes = [u for u in unicodes if int(u, 16) in restrict]

    # Choose order of unicodes (chum actually outputs them in
    # sorted unicode order, but it would be unwise to rely on it).
    if args.order == "shuffle":
        unicodes = shuffle(unicodes)
    elif args.order == "code":
        unicodes = codesort(unicodes)
    elif args.order == "uca":
        unicodes = uca(unicodes)
    else:
        raise Exception("Unknown --order argument: %s" % args.order)

    flowed = flow(unicodes)

    if args.unicodes:
        joined = ",".join(flowed)
        print(joined)
    elif args.chars:
        aschars = "".join(chr(int(u, 16)) for u in unicodes)
        print(aschars)
    else:
        hbview(flowed, rest + [args.fontfile])


def shuffle(l):
    """Randomly shuffle the list l, returning a fresh one."""

    # randomly shuffled
    shuffled = random.sample(l, k=len(l))

    return shuffled


def uca(l):
    """
    Sort according to the Unicode Collation Algorithm.
    The input is a list of hex-strings;
    the output is fresh list.
    """

    # https://gitlab.pyicu.org/main/pyicu
    # If this import fails, try
    # brew install pkg-config icu4c
    # pip install --no-binary=:pyicu: pyicu
    import icu

    collator = icu.Collator.createInstance()
    cs = [chr(int(u, 16)) for u in l]
    css = sorted(cs, key=collator.getSortKey)
    return ["%04X" % ord(c) for c in css]


def codesort(us):
    """
    Sort according to numerical code.
    The input is a list of hex-strings;
    the output is fresh list.
    """

    cs = [int(u, 16) for u in us]
    css = sorted(cs)
    return ["%04X" % c for c in css]


def getcmap(fontfile):
    """Run the external program chum and return output."""

    child = subprocess.run(["chum", fontfile], stdout=subprocess.PIPE)
    out = child.stdout.decode("utf-8")

    lines = out.split()

    return lines


def hbview(unicodes, args):
    """Run the external program hb-view, using *unicodes* for
    its text input (*unicodes* is a sequence of unicode values
    represented as a hex string; it is converted to UTF-8 and
    fed as stdin to `hb-view`).
    *args* is a list of remaining arguments to pass to hb-view;
    if there are no optional arguments to pass to hb-view then
    *args* will be a single element list of [fontfile].
    Output is not redirected, hb-view will output to this
    process's stdout.
    """

    # Works around hb-view truncation bug
    # https://github.com/harfbuzz/harfbuzz/issues/4051
    os.environ["HB_DRAW"] = "0"

    child = subprocess.Popen(
        ["hb-view"] + args, stdin=subprocess.PIPE, stdout=None, encoding="utf-8"
    )

    input = "".join(chr(int(u, 16)) for u in unicodes)
    child.stdin.write(input)
    child.stdin.close()
    child.wait()


def list_unicodes(fontfile):
    """
    Output a sequence of unicode values,
    each one a hex-string,
    that are the valid characters supported by the font
    """

    # The output as a sequence of lines;
    # each line is a unicode hex value.
    unicodes = getcmap(fontfile)

    return unicodes


def flow(unicodes):
    """Insert newlines, 000A, every so often."""

    n_rows = int(math.floor(len(unicodes) ** 0.5))
    out = []

    while n_rows > 0:
        # Number of items in next row
        N = int(math.ceil(len(unicodes) / n_rows))
        out.extend(unicodes[:N])
        out.append("000A")
        unicodes = unicodes[N:]
        n_rows -= 1

    return out


if __name__ == "__main__":
    main()
