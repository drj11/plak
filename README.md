# plak

`plak` generates specimen plaques for font-files.

`plak` will run `hb-view` with input that is
the list of characters in the selected order (usually, shuffled).
The list of characters is extracted from the font-file.

Typical usage:

    plak font.ttf | kitty icat

If you only want the shuffield sequence of Unicode hex codepoints,
which would be suitable for passing to `hb-view --unicodes`,
then run `plak --unicodes font.ttf` which avoids running
`hb-view` and prints the Unicodes lists instead.

`plak` inspects the `cmap` table of a font and generates all
Unicode codepoints that have entries.
It will therefore generally avoid ligatures and other `GSUB`
generated glyphs.
Although it seems that fonts from before the OpenType era put
ligatures in the Private Use Area,
and therefore those ligatures _will_ appear in the output.


## Installing

You will need Python and `git`.

From the top-level directory of a clone of this repository:

    python3 -m pip install .

_Running_ `plak` requires `hb-view` (from harfbuzz) and
`ccmap` (from font8), neither of which are installed automatically.

Running `plak` with the `--order uca` option requires
[the Python library PyICU](https://pypi.org/project/PyICU/).
It may be a bit tricky because even once installed,
`brew` may upgrade the binary ICU library that PyICU depends on;
this moves its location and means that the `PyICU` binding
fails.
Some hints are provided in the _PyICU_ section.


## PyICU

Install icu libraries using homebrew:

    brew install icu4c

The Python `pyicu` also needs installing, and this requires
knowledge of where the `pkgconfig` directory for the icu library is.
That knowledge is passed to `pip install` using the
`PKG_CONFIG_PATH` environment variable:

    PKG_CONFIG_PATH=/usr/local/Cellar/icu4c//72.1/lib/pkgconfig python -m pip install pyicu

But you need to find the right directory.
As per the error message you get when `PKG_CONFIG_PATH` is not set,
the directory containing `icu-i18n.pc` should be added.
I found this directory using:

    find /usr/local/Cellar/icu4c -name icu-i18n.pc

(which gave me two options, i picked the later one)

Previous notes on installing PyICU which may still be helpful:

I used to try this using homebrew,
as per the instructions _on the [PyICU homepage](https://pypi.org/project/PyICU/)_. 

Uninstall `PyICU` and reinstall it.
It seems that i prefer:

    python -m pip uninstall pyicu
    CC="$(which gcc)" CXX="$(which g++)" python -m pip install pyicu

as of 2023 that no longer works.


## Developers

Run tests with

    python3 -m pytest

# END
