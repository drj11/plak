# Tests for plak.py
# run with python3 -m pytest

# local module
import plak


def test_flow0():
    assert len(plak.flow([])) == 0


def test_flow1():
    res = plak.flow(["hi"])
    # a newline element is added
    assert len(res) == 2
    assert int(res[1], 16) == 10


def test_flow26():
    lc = "abcdefghijklmnopqrstuvwxyz"
    assert len(lc) == 26
    res = plak.flow(lc)
    assert len(res) > 26

    nl = res[-1]
    while nl in res:
        res.remove(nl)

    assert len(res) == 26
